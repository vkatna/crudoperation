<?php

namespace App\Http\Controllers;
use App\Models\UserDetail;

use Illuminate\Http\Request;

class UserDetails extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = UserDetail::all();
        return view('userDetails.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('userDetails.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
        'firstname'=>'required',
        'lastname'=>'required',
        'email'=>'required',
    ]);

    $userDetails = new UserDetail([
        "firstname" => $request->get('firstname'),
        "lastname" =>$request->get('lastname'),
        "email" => $request->get('email'),
        "usertype" => $request->get('userType'),
    ]);

    $userDetails->save();
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = UserDetail::find($id);
        return view('userDetails.edit', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required',
        ]);
        $user = UserDetail::find($id);
        $user->firstname =  $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->usertype = $request->get('userType');
        $user->save();
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = UserDetail::find($id);
        $user->delete();
        return redirect('/users');
    }
}
