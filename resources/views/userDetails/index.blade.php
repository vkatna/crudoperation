@extends('layouts.master')
@section('content')
<div class="row">
<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-8">
            <h1 class="display-3">Users</h1>  
        </div>
        <div class="col-sm-4">
            <a class="btn btn-primary" href="{{ route('users.create')}}" >Add User</a>
        </div>
    </div> 
  <table class="table table-striped">
    <thead>
        <tr>
          <td>First Name</td>
          <td>Last Name</td>
          <td>Email</td>
          <td>User Type</td>
          <td colspan=2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->firstname}}</td>
            <td>{{$user->lastname}}</td>
            <td>{{$user->email}}</td>
            <td>@if($user->usertype == 1) User  @else Admin @endif </td>
            <td>
                <a href="{{ route('users.edit',$user->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('users.destroy', $user->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection